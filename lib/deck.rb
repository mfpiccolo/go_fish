class Deck
  attr_reader :remaining_cards

  def initialize
    build_cards
    randomize!
  end

  def randomize!
    @remaining_cards.shuffle!
  end

  def deal!
    @remaining_cards.shift(7)
  end

  def draw!
    @remaining_cards.shift(1)
  end

  private

  def build_cards
    @remaining_cards = []
    rank = ['A','2','3','4','5','6','7','8','9','10','J','Q','K']
    suits = ['Hearts', 'Clubs', 'Spades', 'Diamonds']
     suits.each do |suit|
      rank.each do |i|
        @remaining_cards << i + " of " + suit
      end
    end
  end



end