class Hand
  attr_reader :cards
  
  def initialize(cards)
    @cards = cards
  end

  def four?
    sliced_cards = @cards.map {|card| card.slice(0) }
    sliced_cards.select {|card| sliced_cards.count(card) > 3 }.length > 3   
  end

  def choice
    p @cards
    @cards.pop(1).first[0]
  end

  # def matching(hand)
  #   hand.select {|card| hand[0].count(cards) > 1}
  # end

  def include?(card_choice)
     @cards.include?(card_choice)
  end
end