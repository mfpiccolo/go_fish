require '../lib/player'


player1 = Player.new('Player 1', ["5 of Diamonds", "J of Clubs", "A of Hearts", "K of Clubs", "Q of Diamonds", "J of Clubs", "6 of Hearts"])
computer = Player.new('Computer', ["J of Clubs", "Q of Hearts", "5 of Spades", "7 of Clubs", "4 of Clubs", "5 of Hearts", "8 of Diamonds"])

rank_requested = "J"

puts "#{player1} should return an instance of Player"
puts "#{player1.name} should be 'Player 1'"
puts "#{player1.player_hand} should be 7 cards" 
puts "#{computer.has_card?(rank_requested)} should be true"

computer = Player.new('Computer', ["J of Clubs", "Q of Hearts", "5 of Spades", "7 of Clubs", "4 of Clubs", "5 of Hearts", "8 of Diamonds"])

rank_requested = "A"

puts "#{computer.has_card?(rank_requested)} should be false"

computer = Player.new('Computer', ["J of Hearts", "Q of Hearts", "5 of Spades", "7 of Clubs", "4 of Clubs", "5 of Hearts", "8 of Diamonds"])

rank_requested = "J"

puts "#{computer.match(rank_requested)} should be ['J of Hearts']"

matched_cards = computer.match(rank_requested)

player1.receive_cards(matched_cards)
puts "#{player1.player_hand} should be ['5 of Diamonds', 'J of Clubs', 'A of Hearts', 'K of Clubs', 'Q of Diamonds', 'J of Clubs', '6 of Hearts', 'J of Hearts']"

computer = Player.new('Computer', ["J of Hearts", "Q of Hearts", "5 of Spades", "J of Clubs", "4 of Clubs", "5 of Hearts", "8 of Diamonds"])

matched_cards = computer.match(rank_requested)

computer.give_cards(matched_cards)
puts "#{computer.player_hand} should be ['Q of Hearts', '5 of Spades', '4 of Clubs', '5 of Hearts', '8 of Diamonds']"

puts "#{player1.points} should be 0"

player1.add_point

puts "#{player1.points} should be 1"
