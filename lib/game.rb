class Game
  def initialize(remaining_cards, player_hand, computer_hand)
    @remaining_cards = remaining_cards
    @player_hand = player_hand
    @computer_hand = computer_hand 
  end

  def over?
    @remaining_cards == 0 || @player_hand == 0 || @computer_hand == 0
  end

  def winner
  end
end
