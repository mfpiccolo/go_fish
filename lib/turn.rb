class Turn
  attr_reader :over

  def initialize
    @over = false
  end

  def add_turn_points(hand_score)
    if hand_score == 1
      @turn_points += hand_score
    else
      @turn_points
    end
  end

  def over?
    @over
  end

  def right_choice?

    @over = true
  end
end