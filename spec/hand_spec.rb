require '../lib/hand'

hand = Hand.new(["8 of Diamonds", "5 of Hearts", "Q of Hearts", "6 of Diamonds", "7 of Spades", "9 of Hearts", "4 of Hearts"])
puts "#{hand} should be an instance of Hand"
hand = Hand.new(["8 of Diamonds", "8 of Hearts", "Q of Hearts", "4 of Diamonds", "8 of Spades", "8 of Clubs", "4 of Hearts"])
puts "#{hand.four?} should be true"
hand = Hand.new(["8 of Diamonds", "5 of Hearts", "8 of Hearts", "6 of Diamonds", "7 of Spades", "9 of Hearts", "4 of Hearts"])
puts "#{hand.four?} should be false"
# hand.cards
# puts "#{hand.matching(hand.cards)} should be ['8 of Diamonds', '8 of Hearts']"

puts "#{hand.choice} should be 4"

