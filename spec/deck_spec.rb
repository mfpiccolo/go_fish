require '../lib/deck'

deck = Deck.new
puts "#{deck} should be an instance of Deck"
puts "#{deck.remaining_cards} should be an array of all of the cards, 2-A of hearts, spades, clubs, and diamonds"
puts "#{deck.randomize!} should be an array of all the cards in random order."
puts "#{deck.deal!} should be an array of 7 random cards"
puts "#{deck.remaining_cards.length} should be 45"
puts "#{deck.draw!} should be an array of one random card"
puts "#{deck.remaining_cards.length} should be 44"


