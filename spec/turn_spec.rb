require '../lib/turn'

turn = Turn.new
puts "#{turn} should be an instance of Turn"

puts "#{turn.turn_points} should be 0"

hand_score = 0
puts "#{turn.add_turn_points(hand_score)} should be 0"

hand_score = 1
puts "#{turn.add_turn_points(hand_score)} should be 1"

puts "#{turn.turn_points} should be 1"

puts "#{turn.over?} should be false"