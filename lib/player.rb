class Player
  attr_reader :name, :player_hand, :points

  def initialize(name, player_hand)
    @name = name
    @player_hand = player_hand
    @points = 0
  end

  def add_point
    @points += 1
  end
  

  def has_card?(rank_requested) #of card_choice
    @player_hand.any? { |card| card[0].include?(rank_requested) }
  end

  def match(rank_requested)
    @player_hand.select { |card| card[0].include?(rank_requested) }
  end

  def give_cards(matched_cards)
    @player_hand -= matched_cards
  end

  def receive_cards(matched_cards)
    @player_hand += matched_cards
  end

  def include?(card_choice)
    @player_hand.include?(card_choice)
  end
end