require './lib/deck'
require './lib/hand'
require './lib/game'
require './lib/player'
require './lib/turn'

remaining_cards = 9

deck = Deck.new

puts "Let's play some fish!"

player1_hand = Hand.new(deck.deal!)

puts "Your hand is #{player1_hand.cards}"

computer_hand = Hand.new(deck.deal!)

game = Game.new(remaining_cards, player1_hand, computer_hand)

players = [Player.new("Player 1", player1_hand.cards), Player.new("Computer", computer_hand.cards)]

until game.over?
  players.each do |player|
    if player.name == "Player 1"
      turn = Turn.new
      if player1_hand.four?
        player.add_point
        puts "#{player.name}, you scored a point."
        puts "Your total points are equal to #{player.points}."
      else  
        puts "#{player.name}, which card would you like to steal?"
        puts "Type in the rank of the card you would like, e.g. K for Kings, 8 for eights."
        rank_requested = players[0].player_hand.first[0] #gets.chomp 
      end

      if players[0].has_card?(rank_requested) && players[1].has_card?(rank_requested)
        matched_cards = players[1].match(rank_requested)
        players[0].receive_cards(matched_cards)
        players[1].give_cards(players[1].match(rank_requested))
        puts "You stole these cards: #{matched_cards}.  Your new hand is #{players[0].player_hand}."
        #identify the card then move the card(s) from the computer hand to the player hand
      else  
        puts "Go fish!"
        drawn_card = deck.draw!
        puts "You drew a #{drawn_card.first}."
        puts "#{player.receive_cards(drawn_card)} is your hand."
        #Draw next card from deck and add to players_hand and show player their card.
        p "player one points: '#{player.points}'"
      end
    elsif player.name == "Computer"
      turn = Turn.new
      if computer_hand.four?
        player.add_point
      else
        rank_requested = computer_hand.choice
        #Find what most matches from the computers hand. if no matches then random selection.
      end

      if player.has_card?(rank_requested) && players[1].has_card?(rank_requested)
        matched_cards = players[0].match(rank_requested)
        player.receive_cards(matched_cards)
        players[0].give_cards(players[0].match(rank_requested))
        p "Computer hand: #{player.player_hand.join(", ")}"
        puts "The computer is asking you for #{rank_requested}" 
        p "Computer points: '#{player.points}'" 
      else
        player.receive_cards(deck.draw!)
      end
    end
  end
end

#   check for 4 of a kind and add score

#   puts "Which card would you like to steal from the computer?"
#   Player chooses a card
#   check card against both hands(if player doesn't have, invalid)
#   If computer has card(s), hand it/them over
#   If not, draw card from pile
#     if card from pile is choice, go again
#     else turn over

#   Computer's turn
#   check for 4 of a kind and add score
#   Computer chooses a card and displays choice
#   check card against both hands(if computer doesn't have, invalid)
#   If player has card(s), hand it/them over
#   If not, draw card from pile
#     if card from pile is choice, go again
#     else turn over

# game is over when all cards have been drawn from deck
# winner is the one with the most points
# end
# end